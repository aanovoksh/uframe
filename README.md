![uframe_logo](https://github.com/umass-cs-326/team-uframe/raw/master/docs/images/uframe-logo1.png)

## Project Overview

UFrame is a social media game that challenges users to match frames (pictures) taken by other users around campus. For matching a frame correctly, users will receive points which determine their standing on the UFrame leaderboard. UFrame is optimized for both mobile and desktop browsers.

###The Uframe Team

<strong>Gabriel Markarian</strong> Team Manager </br>

<strong>Paul Edwards</strong> Usability Manager </br>

<strong>Yevgeniy Teleganov</strong> Front End Developer </br>

<strong>Daniel Lipeles</strong> Front End Developer </br>

<strong>Alexey Novokshonov</strong> Project Developer </br>

<strong>Edward Murphy</strong> Back End Developer </br>

View our app at <a href="uframe.no-ip.org">uframe.no-ip.org</a>

## Original Proposal

The proposal is located in `docs/proposal/proposal.md` and `docs/proposal/proposal.pdf` for the powerpoint. Our proposal includes overview of all our team members, a problem statement, a complete description of the product, a timeline for implementation and estimated cost of the project.


## Functional Specification  

The functional/user specification is located in docs/fspec/fspec.md and docs/fspec/fspec.pdf for the powerpoint. Our functional/user specification consists of team organization, including roles and responsibilities, overview of the project, use case scenarios, list of non-goals, flowchart showing paths through the system, and screen-by-screen specification of the system.

## Design Specification

The design specification is located in docs/dspec/dspec.md and docs/dspec/dspec.pdf for the powerpoint. Our design specification document showcases the following: a summary of UFrame, our revision history, a list of external libraries with justifications for their use, a bird's eye view of all the components with explanations of how they fit into our web application environment, and a breakdown of the important components in our bird's eye view.

## Videos

https://drive.google.com/folderview?id=0B7K4rgXIxQhgbnN1WVJJNTJHYUk&usp=sharing    

Slides to accompany our commercial:
https://docs.google.com/presentation/d/1SFxWg_84Fc0yCdKkHvgpkU2r45zCPIkY7UTdl8LH-fI/edit?usp=sharing

## Additional Notes

For server setup, as stated in the README doc of the UFrame app directory:
> You MUST indicate your MySQL database credentials in dbcredentials.json. app.js will read this file for your MySQL credentials.
> There is a dbcredentials.json.example file, so make a dbcredentials.json file in the UFrame directory and edit it according to your current MySQL setup.

--

######Team UFrame
