Follow these instructions to be able to use the database during development. With this temporary implementation, our databases will be local and therefore distinct but will rely on the same queries in the code. This will work great for development but will break in Pauls server.

**Install MySQL**   
type in the following command. when prompted for a root password, choose *student*   
> sudo apt-get install mysql-server libapache2-mod-auth-mysql php5-mysql   
once installed, activate it with this command:   
> sudo mysql_install_db   
finish up by running the mysql setup script   
> sudo /usr/bin/mysql_secure_installation   
> ...it will ask you a bunch of questions, just select yes for all of them if youre not sure   
   
**Create local mysql database**   
from the UFrame/bin/db directory, start mysql with this command, and type in *student* for password   
> mysql -u root -p   
inside mysql, enter the following commands:   
> CREATE DATABASE uframe;   
> USE uframe;   
> SOURCE uframedb.sql;   
after mysql finishes, try this statement:   
> SHOW TABLES;
