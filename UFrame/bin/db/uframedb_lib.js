//THIS LIBRARY IS A PLACE FOR UPPER LEVEL FUNCTIONS USED THROUGHOUT
//THE APP THAT INVOLVE DATABASE QUERIES!
/*Placed used: app.js -- used in local strategy to validate users.
*/

var dbcredentials = require('../../dbcredentials.json');

var mysql = require('mysql'),
    dbOptions = {
      host : 'localhost',
      user : dbcredentials["username"],
      password : dbcredentials["password"],
      port : '3306',
      database : 'uframe'
    };

var connection = mysql.createConnection(dbOptions);
connection.connect();

// cb(err, {user: username}) only use with passport
function userLogin(username, password, cb){

	var q = "SELECT * FROM users WHERE username='"+username+"' and password='"+password+"'";//select user

	connection.query(q, function(err, rows, fields) {
		if (err && rows.length>1){
      return cb(null, false, { message: 'DB Error.' });

		}else if(rows.length===1){
			 return cb(null, {user : username});
		}

		 return cb(null, false, { message: 'Incorrect Username/Password' });

	});
}

//untested
//function getUser(name, cb){
//  var profilepic = '../images/test-profile1.png';   /*db query for profile picture*/
//  var matches    =   [ '../images/test-frame1.png', /*db query for users matches*/
//                              '../images/test-frame1.png',
//                             '../images/test-frame1.png'];
//
//
//
//  if(!name){
//    cb('No user info provided');
//  }
//  else{
//    cb(undefined, { uname: name,
//                    propic: profilepic,
//                    frames: matches });
//  }
//}

function getUserByID(id, cb){
  var profilepic = '/images/test-profile1.png';   /*db query for profile picture*/
  var matches = null;/* db query for profile picture */
  matches = [{
    f_link:  '/',
    f_img_path: '/images/test-frame1.png'
  }, {
    f_link:  '/',
    f_img_path: '/images/test-frame1.png'
  }, {
    f_link:  '/',
    f_img_path: '/images/test-frame1.png'
  }]

  if(!id){
    cb('No user info provided!');
  }
  else{
    var q = "SELECT * FROM users WHERE u_id="+id;
    connection.query(q, function(err, rows){
      if(err) { cb(err); }
      else {
        cb(undefined, { uname:  rows[0].username,
                        propic: profilepic,
                        frames: matches,
                        points: rows[0].rank
        });
      }
    });
  }
}

function getFramebyId(id, cb){
  //to be built out by the good 'ol boys at backend
  var q = "SELECT * FROM frames WHERE f_id="+id;
  connection.query(q, function(err, pic){
    if(pic.length === 1){
      var q2 = "SELECT * FROM matches WHERE f_id_fk="+id;
      connection.query(q2, function(err, rows){
        if(err){ cb(err); }
        else{
          cb(undefined,pic[0], rows);
        }
      });

    }
    else{
      if(pic.length === 0){ /*frame not found*/ }
      else {  /*multpile entries, critical error*/ }
     }
  });
}

function saveFrametoDb(imgPath, hint, username, cb) {
  var q = "SELECT u_id FROM users WHERE username='"+username+"'";
  connection.query(q, function(err, rows) {
    if(err){
      cb(-1);
    }
    else{
//rows[0].u_id
      var post = {u_id_fk: rows[0]['u_id'], f_img_path: imgPath, f_hint: hint};
      connection.query('INSERT INTO frames SET ?', post, function(err, rows1) {
        if(err) cb(-2);
        else {
         var q2 = "REPLACE INTO frames (f_id, f_img_path, f_thumbnail_path, u_id_fk) VALUES ('"+rows1.insertId+"','/frames/f"+rows1.insertId+".png','/thumbnails/t"+rows1.insertId+".png','"+rows[0].u_id+"')";
         connection.query(q2, function(err) {
           if(err) cb(-3);
           else    cb(rows1.insertId);
         });
        }
      });
    }
  });
}

function saveMatchtoDb(imgPath, u_id, f_id, cb){
  var q = "SELECT u_id FROM users WHERE u_id='" + u_id + "'";
  connection.query(q, function(err, rows) {
    if(err){
      cb(-1);
    }
    else{
      var post = {u_id_fk: u_id, m_img_path: imgPath, f_id_fk: f_id, up_score: 0, down_score: 0};
      connection.query('INSERT INTO matches SET ?', post, function(err, rows1) {
        console.log(err);
        if(err) cb(-2);
        else {
          var q2 = "REPLACE INTO matches (m_id, m_img_path, m_thumbnail_path, f_id_fk, u_id_fk) VALUES ('"+rows1.insertId+"','/matches/m"+rows1.insertId+".png','/thumbnails/t"+rows1.insertId+".png','"+f_id+"','"+u_id+"')";
          connection.query(q2, function(err) {
            console.log(err);
            if(err) cb(-3);
            else    cb(rows1.insertId);
          });
        }
      });
    }
  });
}

function getUsers(rows, users, pos, next, done){
  if(rows.length<=pos){
    //console.log("Done");
    done(rows,users);
  }else{
    //console.log("Pos: "+pos+" RL: "+rows.length);
    var q2 = "SELECT username FROM users WHERE u_id="+rows[pos].u_id_fk+"";
    connection.query(q2,function(errr, rows2) {
      if(!errr){
        if(rows2.length===0){
          done(rows,users);
        }
        //console.log(rows2[0].username)
        users.push(rows2[0].username);
        next(rows,users,pos+1,next,done);
      }else{
        //console.log("E: "+err);
      }
    });
  }
}

function getFrames(cb) {
  var q = "SELECT f_img_path,u_id_fk,f_id,f_time FROM frames ORDER BY f_time DESC";
  connection.query(q, function(err, rows) {
    if(!err){
      var users =[];
      getUsers(rows, users, 0, getUsers, cb);
      /*rows.forEach(function(item){
        var q2 = "SELECT username FROM users WHERE u_id="+item.u_id_fk+"";
        connection.query(q2,function(errr, rows2) {
          if(!errr){
            console.log(rows2[0].username)
            users.push(rows2[0].username);
          }else{
            console.log("E: "+err);
          }
        });
      });
      cb(rows,users);*/
    }
  });
}

function query(query , cb){
    connection.query(query, function(err, rows) {
      if(err){
        cb(err);
      }
      cb(undefined,rows);
  });
}

function getMatchesByFId( fid, cb ){
  var q = "SELECT * FROM matches WHERE f_id_fk="+fid;
  connection.query(q, function(err, rows){
    if(err){ cb(err); }
    else{
     cb(undefined, rows);
    }
  });
}

//function getUserByID(id, cb){
//  var q = "SELECT * FROM users WHERE u_id="+id;
//  connection.query(q, function(err, rows){
//    if(err){
//      cb(err);
//    }
//    console.log("ID: "+id+" R: "+rows[0].username);
//    cb(undefined,rows);
//  });
//}

exports.saveMatchtoDb = saveMatchtoDb;
exports.getFrames     = getFrames;
exports.saveFrametoDb = saveFrametoDb;
exports.getFramebyId  = getFramebyId;
exports.userLogin     = userLogin;
exports.query         = query;
exports.getUserByID   = getUserByID;
exports.getMatchesByFId = getMatchesByFId;


