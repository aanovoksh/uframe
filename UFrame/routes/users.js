var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res) {
  res.send('Logged In: User: '+req.session.user);
});

router.post('/', function (req, res) {//Login Route
  var post = req.body;
  if (post.username && post.password){//check if user and pass are good
    req.session.user = post.username;//Should be generated id for the session and logged with a timeout in the server
    res.redirect('/users/');
  }else{
    res.send('No User/Password');
  }
});

router.get('/upload', function(req, res){
  res.render('upload', {title: 'New Frame'});
});


/*  DELETE LATER IF PASSPORT SUCCEEDS
function checkAuth(req, res, next){
  if(!req.session.user){
    res.send('Not Logged In');
  }else{
    next();
  }

}
*/

module.exports = router;
