 var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sass = require('node-sass');
var sassMiddleware = require('node-sass-middleware');
var session = require('express-session');
var passport = require('passport'), LocalStrategy = require('passport-local').Strategy;
var flash=require('connect-flash');
var busboy = require('connect-busboy');

//ROUTES
var routes = require('./routes/index');
var users = require('./routes/users'); //widely unused, see profiles
var frames = require('./routes/f');
var profiles = require('./routes/u');
var friends = require('./routes/friends');
//USERLIB
var uframedb_lib = require('./bin/db/uframedb_lib');
var app = express();

//Cookies!
app.use(cookieParser());
app.use(session({secret: 'andthisframeyoucannotmatch',
			 saveUninitialized : true,
			 resave : true }));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(busboy());
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, '/public/scss'),
  dest: path.join(__dirname, '/public/stylesheets'),
  debug: true,
  prefix: '/stylesheets'
}));
app.use(express.static(path.join(__dirname, 'public')));
  //////////////////////////
 ///PASSPORT MIDDLEWEAR////
//////////////////////////
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


app.post('/login', passport.authenticate('local',{
                         successRedirect: '/feed',
                         successFlash:  'Welcome to UFrame at UMass!',
                         failureRedirect: '/',
                         failureFlash:  'Username or password incorrect*!'
                    })
);

passport.use(new LocalStrategy(function(username, password, done){
    uframedb_lib.userLogin(username,password,done);
}));

passport.serializeUser(function(user, done){
  done(null, user);
});
passport.deserializeUser(function(user, done){
  done(null, user);
});


  ///////////////////
 ///END PASSPORT////
///////////////////

app.use('/', routes);
app.use('/users', users);  //widely unused, see profiles
app.use('/f', frames);
app.use('/u', profiles);
app.use('/friends', friends);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
