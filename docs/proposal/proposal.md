![Alt text](https://github.com/umass-cs-326/team-uframe/blob/master/docs/images/uframe-logo1.png "Uframe Logo")

##The reframe what UFrame game!

###Team Members

> **Yevgeniy Teleganov**  
>Front End Developer / Designer
>
> ----------
>
> I am a junior CS major with a focus in networking. I've interned at a SDN company for the past two years. In that time I've done a lot with both front and back end web development in Ruby on Rails and JavaScript (and the Ember.js framework) for both mobile and desktop environments. I'm pretty good with anything related to design/music/video production and am interested in marketing/advertising.
>
> ##### CS Courses
>> CS 121, 187  
>> CS 220, 230, 240, 250  
>> Currently taking: CS 320, 326, 453
>
> ##### 2 Non-CS Courses
>> Art 105: Studio Design --- Sociology 110: Intro to Sociology
>
> My experience in both my major and non-major courses will most definitely influence my work in developing with my team. Design patterns and optimization from CS220, visual design from Art 105, networking from CS453, etc. will all contribute to my abilities as a front end developer for this team.

----------

> **Alexey Novokshonov**  
>Project Developer
>
> ----------
>
> I am a junior CS major focusing in the Artificial Intelligence. I am good at search techniques, reasoning under uncertainty and logic. I am sure that knowledge of computational theory from CS250, algorithms and decision theory from CS383 and programming experience from other classes will contribute to the project development. 
>
> ##### CS Courses
>>CS 121, 187  
>>CS 220, 230, 240, 250  
>>Currently taking: CS 320, 326, 383  
>
> ##### 2 Non-CS Courses
>> ECON 103: Introduction to Microeconomics --- German 230: Intermediate German
>
> My CS and non-CS classes helps me to broaden my knowledge. It allows me to look at the web app development process from different prospectives: as a developer, as a user, as an economist or even as a venture capitalist firm employee. This experience will help the team to develop the successful web-based application.

----------

> **Paul Edwards**  
>Usability Manager
>
>--------
>
> I am a senior CS-major of Umass Amherst. I have experience with HTML, CSS, JS, PHP, and MYSQL, which will allow me to help design the project. Along with the experience of these languages I have a good knowledge of usability, style, and functionality, to help me manage usability.
>
>##### CS Courses
>> CS 121, 187  
>> CS 220, 230, 240, 250  
>> CS 311, 383  
>> Currently Taking: CS 320, 326, 377, 453  
>
>##### 2 Non-CS Courses
>> Math 235: Linear Algebra --- Phy151: Physics 151  
>
> Both my CS and Non-CS classes will help me with this project, from CS320 soft. eng.(what we are doing) to Linear Algebra and Physics for some of the server work, such as automated image verification software(possibly).


----------

> **Daniel Lipeles**  
>Backend Developer
>
>--------
>
> I am a junior CS major at Umass Amherst looking to specialize in the theory of computation. Along with relevent courswork taken at university, I have also independently studied web-programming and have a working knowledge of the languages associated with it, especially those used in backend development (namely PHP, Javascript, and SQL).
>
>##### CS Courses
>> CS 121, 187
>> CS 220, 230, 240, 250
>> Currently Taking: CS 311, 326, 390MB
>
>##### 2 Non-CS Courses
>>Math 235: Linear Algebra -- Labor 280: Labor & Work in the U.S.
>
> I will utilize knowledge from CS and non-CS courses for this project, I plan to implement design principles learned in 220 as well as using data-structure conventions learned in 187. 

------------

> **Edward Murphy**  
>Front End Developer/Marketing
>
> ----------
>
> I am a third-year undergraduate currently studying computer science at the University of Massachusetts Amherst. My interest in programming began freshman year when I thought that I wanted to be a software engineer. However, my interests have since then gravitated towards the specializations of User Interface, as well as Search and Data Mining. Aside from my interest in computer science, I am a tutor at the UMass Writing Center.
>
> ##### CS Courses
>> CS 121, 187  
>> CS 220, 230, 240, 250  
>> Currently taking: CS 320, 326
>
> ##### 2 Non-CS Courses
>> English 329H "Tutoring Writing: Theory and Practice" --- Math 235 "Linear Algebra"
>
> My experience from computer science and general education courses will contribute to my work with UFrame. I will use the perseverence and determination that I acquired from toughing it through Linear Algebra to work through any tasks that I am given for this project, and I will use my skills with language to help create effective marketing campaigns.

------------

> **Gabe Markarian**  
>Project Manager
>
> ----------
>
> Junior year marks the first semester that I have been able to choose a CMPSCI course and what better way to start off than Web Programming.  My background for programming is much stronger in OO programming like Java but I'm always looking for new knowledge, especially computer science knowledge.  My motto is to take at least one class every semester that makes you leave your comfort zone.  This has led to me to take classes in fields like environmental activism, women's studies, and men's dance.  My career in CS will be in software engineering but if I had it my way, I'd learn it all.
>
> ##### CS Courses
>> CS 121, 187  
>> CS 220, 230, 240, 250  
>> Currently taking: CS 311, 326
>
> ##### 2 Non-CS Courses
>> Psychology 100 "Intro to Psych." --- Nutrition 130 "Nutrition and a Healthy Lifesyle"
>
> I am fortunate enough to be the person who's idea was the basis for the project.  Beyond my major and non-major courses, communication of ideas is very important to me.  I look forward to using refactoring skills from 220 and using the command-line programming experience I picked up in 230.


--

###Problem Statement
 
In today's modernized society, social media and gaming has swept the virtual marketplace. Users are always looking for the newest social experience, and we aim to serve the customer with Uframe. With the ease of access to internet connectivity from a user's smartphone, the possibilities for interaction among friends is limitless.

Given the success of apps such as Snapchat and Clash of Clans, there is a clear potential for a venture in this section of application. With Uframe, we wish to combine the attributes of a social media experience within the context of a fun and simple game. In this way, we are taking a "best of both worlds" approach to design, and creating something that caters to all types of people, whether it be a casual user wanting to play with his or her close friends, or a competetive gamer looking to reach the top of the leaderboards in his locality.

With Uframe we plan to employ ideas taken from classes such as Sociology 110 and Labor 280, with regards to human interaction and competition. We want to create an experience that is enjoyable for everyone and appeals to a certain human desire for social interaction. On a technical level we will utilize the knowledge gained from CS courses such as Programming Methodology and Data Structures to create a solid code base that provides for a seamless end-user experience as well as a safe one that respects and protects the users privacy.  

*Daniel Lipeles 10/01*


--

###Product Description

The goal of our project is to develop, launch, and operate an easy-to-use "social media" game. The game will challenge users to "match" pictures taken by other users around campus. A public leaderboard will show the points for all users, and encourage other to join in on the fun.	
In this game, a user takes a picture on campus and posts it to the UFrame website. From there other users can try and take pictures that match the picture, i.e. The Library. Submissions will recieve points for speed(first to match) and also quality of the match. To determine the quality of each match, there will be a "Not a Match" button on the image, so users can down vote bad matches/no matches.  

*Paul Edwards 10/01*  


--

###Timeline

- **October 2nd through October 9th** Build knowledge base.  Learn working with blobs for photo sharing.  Back end storage and organization of picture files.  Front end establishment of a sign in method for users and what each account should have.  
- **October 10th through October 17th**  Creating and handling a submission form for new posts.  How posts are stored in the bcak end.  How they are retrieved by the front.  
- **October 18th through October 25th**  Establish the matching protocol for Uframe posts.  Ideas include 'not a match' button functionality and personal notifications for when someone attempts a match on one of your posts.  
- **October 26th through November 2nd**  Adding functionality for the 'best match'.  The objective is of course to be the first match, but what about the best?  Match attempts might be listed and voted on by viewers.  
- **November 3rd through November 10th**  Cleanup week.  There will surely be shaky parts of the implementation by now, this week is dedicated to fixing them and making them bulletproof.  
- **November 11th through Finals**  Geotagging functionality.  With a decently robust and smooth running Uframe, a large but worthwhile step in the end might be to track the location of a match-site and alert users' mobile devices when they are near to say 'Uframe match opportunity nearby---Eyes peeled!'  
- **Not listed for specific week** Leaderboard implementation, UI design, mobile device functionality.  

*Gabriel Markarian 10/01*  


--

###Project Cost  

| Item              | monthly cost  |total (3 mo) |
| -------------     |:-------------:| -----------:|
| domain name       | -             |$10          |
| server hosting    | $100          |$300         |
| office lease      | $1,150        |$3,450       |
| technical supplies| -             |$10,000      |
| office furniture  | -             |$2,000       |
| labor expenses    | $12,000       |$36,000      |
| health insurance  | -             |$6,000       |
| unplanned expenses| -             |$1,500       |
| total             |               |$59,260      |

*Alexey Novokshonov 09/30*  

--

######Team UFrame
